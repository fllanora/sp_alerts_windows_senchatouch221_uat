/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // `load`, `deviceready`, `offline`, and `online`.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
	//	  document.addEventListener('resume', this.onResume, false);
    },
    // deviceready Event Handler
    //
    // The scope of `this` is the event. In order to call the `receivedEvent`
    // function, we must explicity call `app.receivedEvent(...);`
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
   // alert("started");
        console.log('Received Event: ' + id);
		
	if (id == "deviceready") {
	
		SP.OfflineCache.initializeFileSystem();
		
		

		
		PARAMS_DEVICE_OS = device.platform;
		console.log("Device OS: " + PARAMS_DEVICE_OS);
		
		
		PARAMS_DEVICE_OS = PARAMS_DEVICE_OS.toLowerCase();
		
		if ((PARAMS_DEVICE_OS == 'ipad') || (PARAMS_DEVICE_OS == 'iphone') || (PARAMS_DEVICE_OS.indexOf('ipod') != -1)) {
			PARAMS_DEVICE_OS = 'iOS';
		}
		if (PARAMS_DEVICE_OS == 'android') {
			PARAMS_DEVICE_OS = 'Android';
		}
		if ((PARAMS_DEVICE_OS == 'win32nt') || (PARAMS_DEVICE_OS == 'wince')) {
			PARAMS_DEVICE_OS = 'Windows';
		}
		
		
		if (device.platform === 'Win32NT') {
			window.alert = window.alert || navigator.notification.alert;
		}
		
		//alert ("test");
		/*
			                      cordova.exec(function(result){
									   console.log("FINAL RESULT:" +result);
									    Ext.Msg.alert("succcess", result);
									}, function(error){ Ext.Msg.alert("error", "fail ajax websso"); }, "WebSSOPlugin", "ajaxWebSSO", ["https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp",SP.WebSSOPlugin.getCookieStoredValue()]);
									
									*/
		                    // SP.WebSSOPlugin.setWebSSODomain('mobileweb-tst.testsf.testsp.edu.sg');
				/*		    SP.WebSSOPlugin.setWebSSODomain('mobileweb.sp.edu.sg');
							SP.PushPlugin.registerDevice(["PushPlugin", "push"], function(result){
									PARAMS_DEVICE_TOKEN = result;
                                   console.log("PARAMS_DEVICE_TOKEN: " +PARAMS_DEVICE_TOKEN);
									cordova.exec(function(webSSOcookie){
									console.log("webSSOcookie: " +webSSOcookie);
									// var requestURL = "https://mobileweb-tst.testsf.testsp.edu.sg/AlertWS/student/alert/AlertPost.jsp?" + PARAMS_DEVICE_TOKEN + "&" + "Windows";
									 var requestURL = "https://mobileweb.sp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";
									console.log("requestURL: " +requestURL);
									 cordova.exec(function(result){
									   console.log("FINAL RESULT:" +result);
									    Ext.Msg.alert("succcess", result);
									}, function(error){ Ext.Msg.alert("error", "fail ajax websso"); }, "WebSSOPlugin", "ajaxWebSSO", [requestURL,webSSOcookie]);
									},function(error){ Ext.Msg.alert("error", "fail form post");  }, "WebSSOPlugin", "formPost", ["WebSSO","p122995","spsdport10"]);
							
								}, function(error){Ext.Msg.alert("error", "fail device register"); 
								});
*/
	                       SP.PushPlugin.registerDevice(["PushPlugin", "push"], function(result){
									PARAMS_DEVICE_TOKEN = result;
                                   console.log("PARAMS_DEVICE_TOKEN: " +PARAMS_DEVICE_TOKEN);
								 }, function(error){});

							
					
                 
           // document.addEventListener("resume", onResume, false);
  
                        
									
	}
		
    }
};




function onResume() {
	
	SP.PushPlugin
	.getMessageId(
			[ "" ],
			function(value) {
				var messageID
				messageID = value;
				if ((messageID === null)
						|| (messageID === "")) {
					PARAMS_FROM_MESSAGEALERT = false;
					processAlerts('', '',
							'');

				} else {

					Ext.getCmp('alertMainView').setActiveItem(0);
					PARAMS_ALERTLOCALSTORAGE = false;
					
					if (messageID == "101") {
						Ext.Viewport
						.setMasked({
							xtype : 'loadmask',
							message : 'Loading...'
						});
						PARAMS_FROM_MESSAGEALERT = false;
						PARAMS_ALERTCATEGORYEXPANDED = "SP LIBRARY";
						setTimeout(function(){processAlerts('', '',
						'');},3000);
					
					} else {

						PARAMS_FROM_MESSAGEALERT = true;
						processMessageAlert(
								'', '', '',
								messageID);

						var rec;
						var checkedValue;
						var currentAlertID = messageID;

						try {
							rec = ReadAlertOfflineStore
									.findRecord(
											'alert_Id',
											currentAlertID);
							checkedValue = rec
									.get('alert_Id');
						} catch (err) {
						}

						console
								.log(">>>>> checkedvalue: "
										+ checkedValue);

						if (checkedValue != currentAlertID) {

							ReadAlertOfflineStore
									.add({
										alert_Id : currentAlertID
									});
							console
									.log("LOG: Read Alert status added to offline store");
						} else {
							console
									.log("LOG: Read Alert status not added to offline store");
						}

						ReadAlertOfflineStore
								.sync();

						SP.PushPlugin
								.setMessageId(
										[ "" ],
										null,
										null);
					}

				}

			}, function(error) {
			});

	
}






app.initialize();