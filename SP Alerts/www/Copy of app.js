

/*****************************************************
 * Body Onload
 *****************************************************/
function onBodyLoad(){
    document.addEventListener("deviceready", onDeviceReady, false);
    document.addEventListener("resume", onDeviceResume, false);
}



/*****************************************************
 * On Device Ready
 *****************************************************/
function onDeviceReady(){
    document.addEventListener("backbutton", backKeyDown, true);
    SP.OfflineCache.initializeFileSystem();
    // Get Application Name
    // if (device.platform == 'Android') {
    PhoneGap.exec(function(result){
        SP.OfflineCache.applicationPackageName = result;
    }, function(result){
    }, "OfflineCacheDownload", "getApplicationPackageName", null);
    //}
    
    PARAMS_DEVICE_OS = device.platform;
    console.log("Device OS: " + PARAMS_DEVICE_OS);
    
    getScreenResolution();
    
    PARAMS_DEVICE_OS = PARAMS_DEVICE_OS.toLowerCase();
    if ((PARAMS_DEVICE_OS == 'ipad') || (PARAMS_DEVICE_OS == 'iphone')) {
        PARAMS_DEVICE_OS = 'iOS';
    }
    if (PARAMS_DEVICE_OS == 'android') {
        PARAMS_DEVICE_OS = 'Android';
    }
    
    PARAMS_DEVICE_TOKEN = "";
    PushPlugin.registerDevice(["PushPlugin", "push"], function(result){
        console.log("devicetoken: " + result);
        /*******************************************************
         * Register Device Token to Push Server
         * @param {String} result Device Token
         *******************************************************/
        PARAMS_DEVICE_TOKEN = result;
    }, function(error){
        // alert("error");
    });
    
    
}

/*****************************************************
 * Back Key Down
 *****************************************************/
function backKeyDown(){
    if (PARAMS_ISALERTDECRIPTIONVIEW) {
        PARAMS_FIRSTLAUNCH = false;
        PARAMS_ISALERTDECRIPTIONVIEW = false;
        if (PARAMS_PUSHALERT_READ == true) {
            PARAMS_DIRECTION = "none";
        }
        else {
            PARAMS_DIRECTION = "back";
        }
        PARAMS_ACTIVEITEM = 0;
		/*****************************************************
        * ISSUE DESCRIPTION/FIX:
        * Implement the functionality to display back
        * the Alerts Menu List when back button (physical button) 
        * is pressed when viewing the Alerts Description  
        * 
        * INCIDENT NUMBER: 
        * IN-SP-2013-0001
        * 
        * UPDATED BY:
        * Fritz Llanora
        * 
        * Date:
        * June 12, 2013
        * 
        *****************************************************/
        Ext.dispatch({  
            controller: app.controllers.myCPController,
            action: 'backToMyCPList'
        });
        processAlerts(PARAMS_STUDENT_ID, true, PARAMS_ALERTCATEGORY_ACTIVE);
        
    }
    else {
        navigator.app.exitApp();
    }
}


/*****************************************************
 * On Device Resume
 *****************************************************/
function onDeviceResume(){

    PushPlugin.getMessageId([""], function(value){
        var messageID = value;
        var currentAlertID = value;
        
        
        PARAMS_PUSHALERT_READ = false;
        
        if ((messageID === null) || (messageID === "")) {
        }
        else {
            PARAMS_PUSHALERT_READ = true;
            if (messageID == "101") {
                Ext.getBody().mask(Ext.LoadingSpinner + '<div class="x-loading-msg">Loading...</div>', 'x-mask-loading', true);
                
                
                PARAMS_DIRECTION = "none";
                PARAMS_ACTIVEITEM = 0;
                PARAMS_ALERTCATEGORY_ACTIVE = "SP LIBRARY";
                PARAMS_LIBRARYACTIVE = true;
                
                try {
                    PushPlugin.setMessageId([""], function(result){
                    }, function(error){
                    });
                } 
                catch (err) {
                }
                
                
                try {
                    Ext.getCmp("MainTabPanel").setActiveItem(0, false);
                } 
                catch (err) {
                }
                
                Ext.getCmp('MainAlertView').setActiveItem(0, {
                    type: 'slide',
                    direction: 'left'
                });
                
                processAlerts(PARAMS_STUDENT_ID, false, PARAMS_ALERTCATEGORY_ACTIVE);
                PARAMS_LIBRARYACTIVE = false;
                
                
            }
            else 
                if (messageID == "102") {
                    Ext.getBody().mask(Ext.LoadingSpinner + '<div class="x-loading-msg">Loading...</div>', 'x-mask-loading', true);
                    try {
                        PushPlugin.setMessageId([""], function(result){
                        }, function(error){
                        });
                    } 
                    catch (err) {
                    }
                    
                }
                else {
                
                    Ext.getBody().mask(Ext.LoadingSpinner + '<div class="x-loading-msg">Loading...</div>', 'x-mask-loading', true);
                    var alertURL = "https://mobileweb-tst.testsf.testsp.edu.sg/AlertWS/student/alert/AlertRequest.jsp";
                    Ext.AlertDataRead = new Ext.data.Store({
                        id: 'AlertStoreId',
                        model: 'Alert',
                        sorters: [{
                            property: 'alert_Source',
                            direction: 'ASC'
                        }],
                        autoLoad: true,
                        proxy: {
                            type: 'ajax',
                            url: alertURL,
                            reader: {
                                type: 'xml',
                                record: 'alerts'
                            },
                            timeout: 15000
                        }
                    });
                    
                    
                    Ext.AlertDataRead.on('load', function(store, recs, success){
                        var currentRecord = store.findRecord("alert_Id", messageID);
                        var alertRecordID = currentRecord.get('alert_Id');
                        var textHtml = currentRecord.get('alert_Description');
                        var alertTitleHtml;
                        var modifiedtextHtml;
                        PARAMS_TIMEOUTSTOPPED = true;
                        
                        PARAMS_ALERTCATEGORY_ACTIVE = currentRecord.get('alert_Source');
                        PARAMS_ISALERTDECRIPTIONVIEW = true;
						
                        if (screenWidth < 600) {
                            var alertTitleHtml = "<font size='2'><center><b>" + currentRecord.get('alert_Title') + "</center></b></font><br><br>";
                            var modifiedtextHtml = alertTitleHtml + "<font size='2'>" + replaceURLWithHTMLLinks(textHtml) + "</font>";
                        }
                        else {
                            var alertTitleHtml = "<font size='4'><center><b>" + currentRecord.get('alert_Title') + "</center></b></font><br><br>";
                            var modifiedtextHtml = alertTitleHtml + "<font size='4'>" + replaceURLWithHTMLLinks(textHtml) + "</font>";
                            
                        }
                        
                        Ext.getCmp('AlertDetailsView').update(modifiedtextHtml);
                        
                        PARAMS_ACTIVEITEM = 1;
                        
                        try {
                            Ext.getCmp("MainTabPanel").setActiveItem(0, false);
                        } 
                        catch (err) {
                        }
                        
                        Ext.getCmp('MainAlertView').setActiveItem(1, {
                            type: 'slide',
                            direction: 'left'
                        });
                        
                        Ext.getBody().unmask();
                        
                        var value = "";
                        var rec = "";
                        var checkedValue = "";
                        
                        var ReadAlertOfflineStore = new Ext.data.Store({
                            model: 'Alert',
                            proxy: {
                                type: 'localstorage',
                                id: 'ReadAlertOfflineStoreLocalStorage'
                            },
                            autoLoad: true
                        });
                        
                        try {
                            rec = ReadAlertOfflineStore.findRecord('alert_Id', currentAlertID);
                            checkedValue = rec.get('alert_Id');
                        } 
                        catch (err) {
                        }
                        
                        console.log(">>>>> checkedvalue: " + checkedValue);
                        
                        if (checkedValue != currentAlertID) {
                        
                            ReadAlertOfflineStore.add({
                                id: currentAlertID,
                                alert_Id: currentAlertID
                            });
                            console.log("LOG: Read Alert status added to offline store");
                        }
                        else {
                            console.log("LOG: Read Alert status not added to offline store");
                        }
                        
                        ReadAlertOfflineStore.sync();
                    }, function(error){
                        alert("error");
                        
                    });
                    
                    
                    try {
                        PushPlugin.setMessageId([""], function(result){
                        }, function(error){
                        });
                    } 
                    catch (err) {
                    }
                    
                }
        }
    }, function(error){
    });
    
}




/*****************************************************
 * Main Application
 *****************************************************/
Ext.regApplication({
    name: "app",
    launch: function(){
        this.views.viewport = new this.views.Viewport();
       SP.WebSSOPlugin.setWebSSODomain('mobileweb-tst.testsf.testsp.edu.sg');
	// SP.WebSSOPlugin.setWebSSODomain('mobileweb.sp.edu.sg');
        Ext.dispatch({
            controller: app.controllers.WebSSOController,
            action: 'startWebSSO'
        });
    }
});




/*****************************************************
 * Check Resolution
 *****************************************************/
function getScreenResolution(){
    screenHeight = document.documentElement.clientHeight;
    screenWidth = document.documentElement.clientWidth;
    screenTempHeight = 0;
    screenTempWidth = 0;
    if (screenHeight <= 0) {
        screenHeight = window.innerHeight;
    }
    if (screenWidth <= 0) {
        screenWidth = window.innerWidth;
    }
    
    screenTempHeight = screenHeight;
    screenTempWidth = screenWidth;
    
    if (screenHeight < screenWidth) {
        screenHeight = screenTempWidth;
        screenWidth = screenTempHeight;
    }
    
    if (screenHeight <= 0) {
        if (device.version == 13) {
            screenHeight = 800;
        }
        else {
            screenHeight = 480;
        }
    }
    
    if (screenWidth <= 0) {
        if (device.version == 13) {
            screenWidth = 700;
        }
        else {
            screenHeight = 320;
        }
    }
}


console.log("LOG: screenHeight: " + screenHeight + " screenWidth: " + screenWidth);
