Ext.define('app.model.alertsModelsModel', {
    extend: 'Ext.data.Model',

    config: {
       fields : [
        {name : 'StaffSelectedPassType', type:'string'},
        {name : 'BookingDateRequest', type:'string'},
        {name : 'BookingStatus', type:'string'},
        {name : 'ReturnedDate', type:'string'},
        {name : 'CollectionDate', type:'string'},
        {name : 'BookingStaffName', type:'string'},
        {name : 'BookingStaffID', type:'string'},
        {name : 'BookingStaffDept', type:'string'},
        {name : 'DocumentUNID', type:'string'}
    ]
    }
});

