Ext.define('app.model.alertsSettingItemModel', {
    extend: 'Ext.data.Model',

    config: {
       fields : [
        {name : 'index', type:'string'},
        {name : 'name', type:'string'}
    ]
    }
});

