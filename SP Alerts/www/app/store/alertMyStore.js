Ext.define('app.store.alertMyStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.alertsModelsModel'
    ],

    config: {
        model: 'app.model.alertsModelsModel',
        autoLoad: true,

        proxy: {
        type: 'ajax',
        url : '',
        reader: {
            type: 'jason',
		    root  : 'MyCorporateRequests'
           

               }
         }
      }   

});