﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using WPCordovaClassLib.Cordova;
using WPCordovaClassLib.Cordova.Commands;
using WPCordovaClassLib.Cordova.JSON;
using System.IO.IsolatedStorage;
using System.Net;

using System.Windows.Navigation;
using Windows.Phone.Storage.SharedAccess;
using System.Diagnostics;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.Phone.Shell;





namespace WPCordovaClassLib.Cordova.Commands
{
    class PushPlugin : BaseCommand
    {

        public static string channelURI = "";
        public static string messageID = "";

        public void registerDevice(string options)
        {
            System.Diagnostics.Debug.WriteLine("registerDevice called");
            channelURI = "";

            try
            {
                channelURI = PhoneApplicationService.Current.State["channeluri"].ToString();
            }
            catch (Exception e) { }


            DispatchCommandResult(new PluginResult(PluginResult.Status.OK, channelURI));
        }

        public void getMessageId(string options)
        {
            System.Diagnostics.Debug.WriteLine("getMessageId called");
            messageID = "";

            try
            {
                messageID = PhoneApplicationService.Current.State["messageid"].ToString();
            }
            catch (Exception e) { }

    
            DispatchCommandResult(new PluginResult(PluginResult.Status.OK, messageID));
        }

     
}

}

